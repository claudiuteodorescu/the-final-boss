import Comments from '/imports/api/comments/collection';

Comments.before.insert(function(userId, comment) {
    comment.userId = userId;
    comment.createdAt = new Date();
});