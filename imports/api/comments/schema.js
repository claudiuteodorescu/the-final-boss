import SimpleSchema from 'simpl-schema';

export default new SimpleSchema({
    text: {
        type: String,
        label: "Comment text",
    },
    userId: {
        optional: true,
        type: String,
    },
    postId: {
        type: String,
    },
    createdAt: {
        optional: true,
        type: String,
    },
});