import {Meteor} from 'meteor/meteor';
import Comments from '/imports/api/comments/collection';
import Security from '/imports/api/security';

Meteor.methods({
    'comment.list': function(postId) {
        return Comments.find({postId}).fetch();
    },

    'comment.add': function(data) {
        Security.checkLoggedIn(this.userId);

        Comments.insert(data);
    },

    'comment.remove': function(_id) {
        const comment = Comments.findOne(_id);

        if (comment.userId !== this.userId) {
            throw new Meteor.Error('not-authorized', 'You are not allowed to remove this comment.');
        }

        Comments.remove(_id);
    },
});