import Posts from '/imports/api/posts/collection';

Posts.before.insert(function(userId, post) {
    post.userId = userId;
    post.createdAt = new Date();
    post.updatedAt = new Date();
});

Posts.before.update(function(userId, post, fieldNames, modifier) {
    modifier.$set = modifier.$set || {};
    modifier.$set.updatedAt = new Date();
});