import SimpleSchema from 'simpl-schema';

export default new SimpleSchema({
    title: {
        type: String,
        label: "Title",
    },
    description: {
        type: String,
        label: "Description",
    },
    createdAt: {
        // This should not be optional but was set optional because
        // the hook that inserts the field is triggered after the schema validation,
        // so if not optional, the schema validation will throw error.
        // Same for updatedAt and userId fields.
        optional: true,
        type: Date,
    },
    updatedAt: {
        optional: true,
        type: Date,
    },
    userId: {
        optional: true,
        type: String,
    },
});