import {Meteor} from 'meteor/meteor';
import Posts from '/imports/api/posts/collection';
import Security from '/imports/api/security';

Meteor.methods({
    'post.create': function(data) {
        Security.checkLoggedIn(this.userId);

        Posts.insert(data);
    },

    'post.get': function(_id) {
        return Posts.findOne(_id);
    },

    'post.edit': function(_id, data) {
        Security.checkLoggedIn(this.userId);

        const post = Posts.findOne(_id);

        if (!post) {
            throw new Meteor.Error('not-found', "The requested post doesn't exist.");
        }

        if (post.userId !== this.userId) {
            throw new Meteor.Error('not-authorized', 'You are not allowed to edit this post.');
        }

        Posts.update(_id, {$set: data});
    },

    'post.remove': function(_id) {
        Security.checkLoggedIn(this.userId);

        const post = Posts.findOne(_id);

        if (!post) {
            throw new Meteor.Error('not-found', "The requested post doesn't exist.");
        }

        if (post.userId !== this.userId) {
            throw new Meteor.Error('not-authorized', 'You are not allowed to delete this post.');
        }

        Posts.remove(_id);
    }
});