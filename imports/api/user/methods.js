import {Meteor} from 'meteor/meteor';
import {Accounts} from 'meteor/accounts-base';

Meteor.methods({
    'user.register': function({email, password}) {
        if (Accounts.findUserByEmail(email)) {
            throw new Meteor.Error(
                'email-already-exists',
                'This email has already been registered.'
            );
        }

        Accounts.createUser({email, password});
    }
});