import route from '/imports/routing/router.js';
import Login from '/imports/ui/pages/user/Login.jsx';
import Register from '/imports/ui/pages/user/Register.jsx';
import PostCreate from '/imports/ui/pages/post/PostCreate.jsx';
import PostList from '/imports/ui/pages/post/PostList.jsx';
import PostEdit from '/imports/ui/pages/post/PostEdit.jsx';
import PostView from '/imports/ui/pages/post/PostView.jsx';

route('/login', Login);

route('/register', Register);

route('/post/create', PostCreate);

route('/post/list', PostList);

route('/post/:postId', PostView);

route('/post/edit/:postId', PostEdit);