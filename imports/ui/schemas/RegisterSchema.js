import SimpleSchema from 'simpl-schema';

SimpleSchema.setDefaultMessages({
  messages: {
    en: {
      "passwordMismatch": "The password and confirmation password do not match",
    },
  },
});

export default new SimpleSchema({
    email: SimpleSchema.RegEx.Email,
    password: {
        type: String,
        min: 5,
    },
    confirmedPassword: {
        type: String,
        label: "Password confirmation",
        min: 5,
        custom() {
            if (this.value !== this.field('password').value) {
                return "passwordMismatch";
            }
        },
    },
});