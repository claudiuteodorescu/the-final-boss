import PostSchema from '/imports/api/posts/schema';

export default PostSchema.pick('title', 'description');