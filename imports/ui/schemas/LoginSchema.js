import SimpleSchema from 'simpl-schema';

export default new SimpleSchema({
    email: SimpleSchema.RegEx.Email,
    password: String,
});