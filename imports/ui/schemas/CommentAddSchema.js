import CommentSchema from '/imports/api/comments/schema';

export default CommentSchema.pick('text', 'postId');