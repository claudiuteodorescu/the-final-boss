import React from 'react';
import {withTracker} from 'meteor/react-meteor-data';

const App = ({main, routeProps, currentUserId}) => {
    const handleLogout = () => {
        Meteor.logout(err => {
            if (err) {
                return alert(err.reason);
            }
        });
    }

    // main represents the component to render passed from the router
    // route props represent the properties that it receives from the router
    
    // where we do createElement, that's where your components will get rendered.
    return (
        <div id="app">
            <div>
                <a href='/post/list'>Posts</a>
                &nbsp;

                {
                    currentUserId
                    ?
                    <span>
                        <a href='/post/create'>Create Post</a>
                        &nbsp;
                        <a href='' onClick={handleLogout}>Logout</a>
                    </span>
                    :
                    <span>
                        <a href='/login'>Login</a>
                        &nbsp;
                        <a href='/register'>Register</a>
                    </span>
                }
            </div>

            {React.createElement(main, routeProps)}
        </div>
    )
};

export default withTracker(props => {
    return _.extend(
        {},
        props,
        {
            currentUserId: Meteor.userId()
        }
    );
})(App);