import React from 'react';
import AutoForm from 'uniforms-unstyled/AutoForm';
import RegisterSchema from '/imports/ui/schemas/RegisterSchema';
import route from '/imports/routing/router.js';

const Register = () => {
    const onSubmit = (formData) => {
        Meteor.call('user.register', formData, (err) => {
            if (err) {
                return alert(err.reason);
            }

            route.go('/login');
        });
    };

    return (
        <div>
            <h2>Register</h2>

            <AutoForm schema={RegisterSchema} onSubmit={onSubmit} />
        </div>
    );

};

export default Register;