import React from 'react';
import AutoForm from 'uniforms-unstyled/AutoForm';
import LoginSchema from '/imports/ui/schemas/LoginSchema';
import route from '/imports/routing/router.js';

const Login = () => {
    const onSubmit = ({email, password}) => {
        Meteor.loginWithPassword(email, password, (err) => {
            if (err) {
                return alert(err.reason);
            }

            route.go('/post/list');
        });
    };

    return (
        <div>
            <h2>Login</h2>

            <AutoForm schema={LoginSchema} onSubmit={onSubmit} />
        </div>
    );
};

export default Login;