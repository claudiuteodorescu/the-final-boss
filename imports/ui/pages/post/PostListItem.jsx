import React from 'react';
import PropTypes from 'prop-types';
import route from '/imports/routing/router.js';

const PostListItem = ({post}) => {
    const currentUserId = Meteor.userId();

    const handleEdit = () => route.go('/post/edit/' + post._id);

    const handleDelete = () => {
        if (!confirm('Are you sure you delete this post?')) {
            return;
        }

        Meteor.call('post.remove', post._id, (err) => {
            if (err) {
                alert(err.reason);
            }
        });
    }

    return (
        <div>
            <a href={'/post/' + post._id}>{post.title}</a>
            &nbsp;
            {
                post.userId === currentUserId
                ?
                <span>
                    <button onClick={handleEdit}>edit</button>
                    &nbsp;
                    <button onClick={handleDelete}>delete</button>
                </span>
                :
                null
            }
        </div>
    );
};

PostListItem.propTypes = {
    post: PropTypes.object.isRequired
}

export default PostListItem;