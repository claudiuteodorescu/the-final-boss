import React from 'react';
import AutoForm from 'uniforms-unstyled/AutoForm';
import AutoFields from 'uniforms-unstyled/AutoFields';
import ErrorsField from 'uniforms-unstyled/ErrorsField';
import SubmitField from 'uniforms-unstyled/SubmitField';
import PostCreateSchema from '/imports/ui/schemas/PostCreateSchema';

export default class PostEdit extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        Meteor.call('post.get', this.props.postId, (err, post) => {
            if (err) {
                return;
            }

            this.setState({post});
        })
    }

    handleSubmit(formData) {
        Meteor.call('post.edit', this.props.postId, formData, (err) => {
            if (err) {
                return alert(err.reason);
            }

            alert('Post successfully updated.');
        });
    };

    render() {
        const post = this.state.post;

        if (!post) {
            return null;
        }

        return (
            <div>
                <h1>Edit Post</h1>

                <AutoForm schema={PostCreateSchema} onSubmit={this.handleSubmit} model={post}>
                    <AutoFields />

                    <ErrorsField />

                    <SubmitField value="Save" />
                </AutoForm>
            </div>
        );
    }
};