import React from 'react';
import Comment from '/imports/ui/pages/post/Comment';
import CommentAdd from '/imports/ui/pages/post/CommentAdd';

export default class CommentView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};

        this.handleFetchComments = this.handleFetchComments.bind(this);
    }

    componentDidMount() {
        this.handleFetchComments();
    }

    handleFetchComments() {
        Meteor.call('comment.list', this.props.postId, (err, comments) => {
            if (err) {
                return;
            }

            this.setState({comments});
        });
    };

    render() {
        if (!Meteor.userId()) {
            return null;
        }

        const postId = this.props.postId;
        const comments = this.state.comments || [];
        const newCommentAdded = this.state.newCommentAdded;

        return (
            <div>
                <p><strong>Comments</strong></p>

                {
                    comments.map(comment => {
                        return (
                            <Comment
                                key={comment._id}
                                comment={comment}
                                onDelete={this.handleFetchComments}
                            />
                        );
                    })
                }

                <CommentAdd postId={postId} onAdd={this.handleFetchComments} key={this.commentAddKey} />
            </div>
        );
    }
};