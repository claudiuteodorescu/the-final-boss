import React from 'react';
import AutoForm from 'uniforms-unstyled/AutoForm';
import AutoField from 'uniforms-unstyled/AutoField';
import LongTextField from 'uniforms-unstyled/LongTextField';
import ErrorsField from 'uniforms-unstyled/ErrorsField';
import SubmitField from 'uniforms-unstyled/SubmitField';
import PostCreateSchema from '/imports/ui/schemas/PostCreateSchema';
import route from '/imports/routing/router.js';

const PostCreate = () => {
    const handleSubmit = (formData) => {
        Meteor.call('post.create', formData, (err) => {
            if (err) {
                return alert(err.reason);
            }

            alert('Post successfully created.');

            route.go('/post/list');
        });
    };

    return (
        <div>
            <h1>Create Post</h1>
            
            <AutoForm schema={PostCreateSchema} onSubmit={handleSubmit}>
                <AutoField name="title" />
                <LongTextField name="description" />

                <ErrorsField />

                <SubmitField />
            </AutoForm>
        </div>
    );
};

export default PostCreate;