import React from 'react';
import AutoForm from 'uniforms-unstyled/AutoForm';
import LongTextField from 'uniforms-unstyled/LongTextField';
import HiddenField from 'uniforms-unstyled/HiddenField';
import ErrorsField from 'uniforms-unstyled/ErrorsField';
import SubmitField from 'uniforms-unstyled/SubmitField';
import CommentAddSchema from '/imports/ui/schemas/CommentAddSchema';

const CommentAdd = ({postId, onAdd}) => {
    let formRef;

    const handleSubmit = (formData) => {
        Meteor.call('comment.add', formData, (err) => {
            if (err) {
                return alert(err.reason);
            }

            formRef.reset();

            if (onAdd) {
                onAdd();
            }
        });
    };

    if (!Meteor.userId()) {
        return null;
    }

    return (
        <AutoForm
            ref={ref => formRef = ref}
            schema={CommentAddSchema}
            onSubmit={handleSubmit}
        >
            <LongTextField name="text" label={null} />
            <HiddenField name="postId" value={postId} />

            <ErrorsField />

            <SubmitField value="Add Comment" />
        </AutoForm>
    );
};

export default CommentAdd;