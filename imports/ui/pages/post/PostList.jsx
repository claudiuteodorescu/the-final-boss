import React from 'react';
import PropTypes from 'prop-types';
import PostListItem from '/imports/ui/pages/post/PostListItem';
import Posts from '/imports/api/posts/collection';

import {withTracker} from 'meteor/react-meteor-data';

const PostList = ({posts}) => {
    return (
        <div>
            <h1>Posts</h1>
            
            {
                posts.map(post => <PostListItem key={post._id} post={post} />)
            }
        </div>
    );
};

PostList.propTypes = {
    posts: PropTypes.array
}

export default withTracker(() => {
    const handle = Meteor.subscribe('posts');

    return {
        loading: !handle.ready(),
        posts: Posts.find().fetch(),
    }
})(PostList);