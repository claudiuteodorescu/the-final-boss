import React from 'react';

const Comment = ({comment, onDelete}) => {
    const isCommentAuthor = comment.userId === Meteor.userId();

    const handleCommentDelete = () => {
        Meteor.call('comment.remove', comment._id, (err) => {
            if (err) {
                return alert(err.reason);
            }

            if (onDelete) {
                onDelete();
            }
        });
    };

    return (
        <p>
            {comment.text}
            &nbsp;
            ({comment.createdAt.toLocaleString()})
            &nbsp;
            {
                isCommentAuthor
                ?
                <button onClick={handleCommentDelete}>delete</button>
                :
                null
            }
        </p>
    );
};

export default Comment;