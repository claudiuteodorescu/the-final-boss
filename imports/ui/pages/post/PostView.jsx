import React from 'react';
import CommentView from '/imports/ui/pages/post/CommentView';

export default class PostView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    componentDidMount() {
        Meteor.call('post.get', this.props.postId, (err, post) => {
            this.setState({post});
        });
    }

    render() {
        const post = this.state.post;

        if (!post) {
            return null;
        }

        return (
            <div>
                <h1>{post.title}</h1>

                <div><i>Posted on {post.createdAt.toLocaleString()}</i></div>

                <p style={{whiteSpace: 'pre-wrap'}}>{post.description}</p>

                <CommentView postId={post._id} />
            </div>
        );
    }
};