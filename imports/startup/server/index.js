import '/imports/api/comments/hooks';
import '/imports/api/comments/methods';

import '/imports/api/posts/hooks';
import '/imports/api/posts/methods';
import '/imports/api/posts/publication';

import '/imports/api/user/methods';